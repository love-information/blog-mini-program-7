// app.js
App({
  onLaunch() {
    var _this=this;
    // 展示本地存储能力
    const logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // // 登录
    // wx.login({
    //   success: res => {
    //     // 发送 res.code 到后台换取 openId, sessionKey, unionId
    //   }
    // })
  },
  login(callback){
     // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认
    // 开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (e) => {
        console.log(1111,e);
        // this.setData({
        //   userInfo: e.userInfo,
        //   hasUserInfo: true
        // })
        console.log(222)
        wx.login({
          success: (res) => {
            console.log(res);
            wx.request({
              url: 'https://www.haigeplus.cn/api/WxLogin/getOpenID',
              data: {
                code: res.code,
                avatarUrl:e.userInfo.avatarUrl,
                nickName:e.userInfo.nickName
              },
              success: (open) => {
                console.log(open.data);
                wx.setStorage({
                  key:'session_token',
                  data:{
                    session3rd:open.data.session3rd,
                    userInfo: e.userInfo
                  }
                })
                callback();
                // wx.getStorage({//获取本地缓存
                //   key:'session_token',
                //   success:function(res){
                //     that.setData({
                //       testNum:res.data
                //     })
                //     console.log(that.data.testNum)
                //   },
                // })
              }
            })
          }
        })
      }
    })
  },
  /**
     * 设置监听器
     */
    setWatcher(data, watch) { // 接收index.js传过来的data对象和watch对象
      Object.keys(watch).forEach(v => { // 将watch对象内的key遍历
          this.observe(data, v,watch[v]); // 监听data内的v属性，传入watch内对应函数以调用
      })
  },

  /**
   * 监听属性 并执行监听函数
   */
  observe(obj, key,watchFun) {
      var val = obj[key]; // 给该属性设默认值
      Object.defineProperty(obj, key, {
          configurable: true,
          enumerable: true,
          set: function(value) {
              val = value;
              watchFun(value,val); // 赋值(set)时，调用对应函数
          },
          get: function() {
              return val;
          }
      })
  },
  globalData: {
    userInfo: null
  }
})
