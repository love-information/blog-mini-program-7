// pages/personal/personal.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    hasUserInfo: false,
    canIUseGetUserProfile: false,
    testNum:[]//设置测试参数
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this;
    wx.getStorage({//获取本地缓存
      key:'session_token',
      success:function(res){
        that.setData({
          hasUserInfo: true,
          testNum:res.data
        })
        console.log(that.data.testNum)
      },
    })
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
  },
  //退出登录
  gotoSubmit(){
    var that=this;
    this.setData({
      hasUserInfo: false
    })
    wx.removeStorage({
      key: 'session_token',
      success:function(res){
        console.log(res)
      }
    })
  },
  getUserProfile(e) {
    var that=this;
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认
    // 开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (e) => {
        console.log(1111,e);
        this.setData({
          userInfo: e.userInfo,
          hasUserInfo: true
        })
        wx.login({
          success: (res) => {
            console.log(res);
            wx.request({
              url: 'https://www.haigeplus.cn/api/WxLogin/getOpenID',
              data: {
                code: res.code,
                avatarUrl:e.userInfo.avatarUrl,
                nickName:e.userInfo.nickName
              },
              success: (open) => {
                console.log(open.data);
                wx.setStorage({
                  key:'session_token',
                  data:{
                    session3rd:open.data.session3rd,
                    userInfo: e.userInfo
                  }
                })
                wx.getStorage({//获取本地缓存
                  key:'session_token',
                  success:function(res){
                    that.setData({
                      testNum:res.data
                    })
                    console.log(that.data.testNum)
                  },
                })
              }
            })
          }
        })
      }
    })
  },
  getUserInfo(e) {
    // 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.onLoad();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})