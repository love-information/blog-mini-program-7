// pages/category_article/category_article.js
const util = require('../../utils/util.js')
const utilList=require('../../utils/netWork.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    articleList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.stopPullDownRefresh() //刷新完成后停止下拉刷新动效
    console.log(111111,options.category_id,2222,options.category_name);
    wx.setNavigationBarTitle({
      title: options.category_name
   })
    let _this=this;
    utilList.request({
      url:'https://www.haigeplus.cn/api/Article/list/page_size/20/category_id/'+options.category_id,
      success:function(res){
          console.log(res);
          var date=[];
          for(let i=0;i<res.data.data.articleList.length;i++){
            date.push(util.formatTime(new Date(res.data.data.articleList[i].add_time*1000)));
          }
          _this.setData({
            articleList:res.data.data.articleList,
            date:date
          })  
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this;
    that.setData({
      currentTab: 0 //当前页的一些初始数据，视业务需求而定
    })
    this.onLoad(this.options); //重新加载onLoad()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

   /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    const { info, code } = that.data;
    return {
      title: info.name,
      path: `/pages/share/index?code=${code}&uid=${wx.getStorageSync('uid')}`,
      imageUrl: info.thumbnail_url
    }
  },
  onShareTimeline: function(){
    const { info, code } = that.data;
    return {
      title: info.name,
      query: `code=${code}&uid=${wx.getStorageSync('uid')}`,
      imageUrl: info.thumbnail_url
    }
  }
})