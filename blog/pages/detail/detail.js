// pages/detail/detail.js
const util = require('../../utils/util.js')
// let dianzan =false
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgUrl:"../../images/dianzan-no.png",
    cover_image:'',//封面图
      article_title:'',
      content:'',
      comment:'',
      upvote:'',//点赞数
      comment_total:'',//评论数
      collect_total:'',//收藏数
      share_total:'',//分享数
      commentList:[],
      testNum:[]
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.stopPullDownRefresh() //刷新完成后停止下拉刷新动效
    var _this=this;
    getApp().setWatcher(this.data, this.watch); // 设置监听器
    // console.log(2222,options.article_id)
            wx.getStorage({//获取本地缓存
              key:'session_token',
              success:function(res){
                _this.setData({
                  testNum:res.data
                })
                console.log(_this.data.testNum)
                 //判断是否点赞
                   wx.request({
                       url: 'https://www.haigeplus.cn/api/Dianzan/list',
                      data:{
                          article_id:options.article_id,
                          session3rd:_this.data.testNum.session3rd
                            },
                      success:function(res){
                              console.log(777,res.data)
                              if(res.data.status==9001){
                                _this.setData({
                                  imgUrl:"../../images/dianzan-yes.png"
                               }) 
                              }
                            }
                            })
              },
            })
    wx.request({
      url:'https://www.haigeplus.cn/api/Article/detail?article_id='+options.article_id,
      success:function(res){
        console.log(333,res)
        var date=[];  
          date.push(util.formatDate(new Date(res.data.data.article.add_time*1000)));
          console.log(11111,res.data.data.article);
          _this.setData({
           article_title:res.data.data.article.article_title,
           content:res.data.data.article.content.replace(/\<img/gi, '<img style="max-width:100%;height:auto" '),
           upvote:res.data.data.article.upvote,//点赞数
           collect_total:res.data.data.article.collect_total,//收藏数
           share_total:res.data.data.article.share_total,//分享数
           cover_image:res.data.data.article.cover_image,
          //  comment_total:res.data.data.article.comment_total,//评论数
          //  imgUrl:dianzan ? "../../images/dianzan-yes.png" : "../../images/dianzan-no.png",
           data:date
          }) 
      }
    })
    wx.request({
      url:'https://www.haigeplus.cn/api/comment/list?article_id='+options.article_id,
      success:function(res){
        console.log(555,res)
        var date=[];
        for(let i=0;i<res.data.data.commentData.length;i++){
          date.push(util.formatTime(new Date(res.data.data.commentData[i].add_time*1000)));
        }
        _this.setData({
          comment_total:res.data.data.commentData.length,//评论数
          commentList:res.data.data.commentData,
          date:date
        })  
      }
    })
  },
  //点赞
  clickMe:function(event){
    var that=this
    let   article_id=that.options.article_id
    // this.setData({
    //   imgUrl:dianzan ? "../../images/dianzan-no.png" : "../../images/dianzan-yes.png"
    // })
    // dianzan=!dianzan
    if(that.data.imgUrl=="../../images/dianzan-no.png"){
      if(that.data.testNum.session3rd){
        wx.request({
          url: 'https://www.haigeplus.cn/api/Dianzan/add',
          data:{
            article_id:article_id,
            session3rd:that.data.testNum.session3rd
          },
          success:function(res){
            console.log(333,res.data)
              that.setData({
                imgUrl:"../../images/dianzan-yes.png",
                upvote:res.data.data.dianzans
              }) 
              wx.showToast({
                title: '点赞成功',//提示文字
                duration:1000,//显示时长
                mask:true,//是否显示透明蒙层，防止触摸穿透，默认：false  
                icon:'success', //图标，支持"success"、"loading"  
                success:function(){ },//接口调用成功
                fail: function () { },  //接口调用失败的回调函数  
                complete: function () { } //接口调用结束的回调函数  
             })
          }
        })
      }else{
        var that=this;
        let shuaxin=function(){
          that.onLoad(that.options);
        }
        getApp().login(shuaxin); // 登录
        // wx.showModal({
        //   title: '提示',
        //   content: '请先登录',
        //  showCancel: true,//是否显示取消按钮
        //  cancelText:"否",//默认是“取消”
        // //  cancelColor:'skyblue',//取消文字的颜色
        //  confirmText:"是",//默认是“确定”
        // //  confirmColor: 'black',//确定文字的颜色
        // })
      }
    }else{
      //取消点赞
      wx.request({
        url: 'https://www.haigeplus.cn/api/Dianzan/delete',
        data:{
          article_id:article_id,
          session3rd:that.data.testNum.session3rd
        },
        success:function(res){
          console.log(333,res.data)
            that.setData({
              imgUrl:"../../images/dianzan-no.png",
              upvote:res.data.data.dianzans
            }) 
        }
      })
    }
  },
  //监听评论内容
  ins:function(e){
    this.setData({
      comment:e.detail.value
    })
  },
  //评论
  commentAdd:function(event){
    var that=this
      let   article_id=that.options.article_id
      let   comment=this.data.comment
      console.log(article_id,comment)
      if(this.data.testNum.session3rd){
        wx.request({
          url:'https://www.haigeplus.cn/api/comment/add',
          data:{
            article_id:article_id,
            comment_content:comment,
            session3rd:this.data.testNum.session3rd,
            nickName:this.data.testNum.userInfo.nickName,
            avatarUrl:this.data.testNum.userInfo.avatarUrl
          },
          success:function(res){
            console.log(333,res.data)
            // var date=[];  
            //   date.push(util.formatTime(new Date(res.data.data.article.add_time*1000)));
            //   console.log(11111,res.data.data.article);
            that.data.commentList.unshift(res.data.data.comment)
              that.setData({
              commentList:that.data.commentList,
              comment_total:that.data.commentList.length,//评论数 添加评论成功重新赋值
              comment:""
              })
          }
        })
      }else{
        var that=this;
        let shuaxin=function(){
          that.onLoad(that.options);
        }
        getApp().login(shuaxin); // 登录
        // wx.showModal({
        //   title: '提示',
        //   content: '请先登录',
        //  showCancel: true,//是否显示取消按钮
        //  cancelText:"否",//默认是“取消”
        // //  cancelColor:'skyblue',//取消文字的颜色
        //  confirmText:"是",//默认是“确定”
        // //  confirmColor: 'skyblue',//确定文字的颜色
        // })
      }
  },
  watch:{
    comment_total:function(newValue){
        console.log(newValue); // comment_total改变时，调用该方法输出新值。
    }
},
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this;
    that.setData({
      currentTab: 0 //当前页的一些初始数据，视业务需求而定
    })
    this.onLoad(this.options); //重新加载onLoad()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  },
  /**
   * 用户收藏
   */
onAddToFavorites:function(){
  var that = this;
  let   article_id=that.options.article_id
  if(this.data.testNum.session3rd){
    console.log(11111);
    wx.request({
      url:'https://www.haigeplus.cn/api/Collect/add',
      data:{
        article_id:article_id,
        session3rd:this.data.testNum.session3rd
      },
      success:function(res){
        console.log(333,res.data)
        // var date=[];  
        //   date.push(util.formatTime(new Date(res.data.data.article.add_time*1000)));
        //   console.log(11111,res.data.data.article);
        // that.data.commentList.unshift(res.data.data.comment)
        //   that.setData({
        //   commentList:that.data.commentList,
        //   comment_total:that.data.commentList.length,//评论数 添加评论成功重新赋值
        //   comment:""
        //   })
      }
    })
  }else{
    wx.showModal({
      title: '提示',
      content: '请先登录',
     showCancel: true,//是否显示取消按钮
     cancelText:"否",//默认是“取消”
    //  cancelColor:'skyblue',//取消文字的颜色
     confirmText:"是",//默认是“确定”
    //  confirmColor: 'skyblue',//确定文字的颜色
    })
  }
},
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    const promise = new Promise(resolve => {
      setTimeout(() => {
        resolve({
          title: this.data.article_title,
          imageUrl:this.data.cover_image
        })
      }, 100)
    })
    var that = this;
    let   article_id=that.options.article_id
    if(this.data.testNum.session3rd){
      wx.request({
        url:'https://www.haigeplus.cn/api/Share/add',
        data:{
          article_id:article_id,
          session3rd:this.data.testNum.session3rd
        },
        success:function(res){
          console.log(333,res.data)
          // var date=[];  
          //   date.push(util.formatTime(new Date(res.data.data.article.add_time*1000)));
          //   console.log(11111,res.data.data.article);
          // that.data.commentList.unshift(res.data.data.comment)
          //   that.setData({
          //   commentList:that.data.commentList,
          //   comment_total:that.data.commentList.length,//评论数 添加评论成功重新赋值
          //   comment:""
          //   })
        }
      })
    }else{
      wx.showModal({
        title: '提示',
        content: '请先登录',
       showCancel: true,//是否显示取消按钮
       cancelText:"否",//默认是“取消”
      //  cancelColor:'skyblue',//取消文字的颜色
       confirmText:"是",//默认是“确定”
      //  confirmColor: 'skyblue',//确定文字的颜色
      })
    }
    return {
      title: this.data.article_title,
      path: '/page/user?id=123',
      imageUrl:this.data.cover_image,
      promise 
    }
  },
  onShareTimeline: function(){
    var that = this;
    let   article_id=that.options.article_id
    if(this.data.testNum.session3rd){
      console.log(11111);
      wx.request({
        url:'https://www.haigeplus.cn/api/Share/add',
        data:{
          article_id:article_id,
          session3rd:this.data.testNum.session3rd
        },
        success:function(res){
          console.log(333,res.data)
          // var date=[];  
          //   date.push(util.formatTime(new Date(res.data.data.article.add_time*1000)));
          //   console.log(11111,res.data.data.article);
          // that.data.commentList.unshift(res.data.data.comment)
          //   that.setData({
          //   commentList:that.data.commentList,
          //   comment_total:that.data.commentList.length,//评论数 添加评论成功重新赋值
          //   comment:""
          //   })
        }
      })
    }else{
      wx.showModal({
        title: '提示',
        content: '请先登录',
       showCancel: true,//是否显示取消按钮
       cancelText:"否",//默认是“取消”
      //  cancelColor:'skyblue',//取消文字的颜色
       confirmText:"是",//默认是“确定”
      //  confirmColor: 'skyblue',//确定文字的颜色
      })
    }
    return {
      title: this.data.article_title,
      // query: `code=${code}&uid=${wx.getStorageSync('uid')}`,
      imageUrl:this.data.cover_image
    }
  }
})