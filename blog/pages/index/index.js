// pages/index/index.js
const util = require('../../utils/util.js')
const utilList=require('../../utils/netWork.js')
Page({
  /**
   * 页面的初始数据
   */
  data: {
    inputValue: '', //点击结果项之后替换到文本框的值
    adapterSource: ["weixin", "wechat", "android", "Android", "IOS", "java", "javascript", "微信小程序", "微信公众号", "微信开发者工具"], //本地匹配源
    bindSource: [], //绑定到页面的数据，根据用户输入动态变化
    hideScroll: true,
    navbarActiveIndex: 0,
    navbarTitle: [],
    categoryList:[],
    commentList:[],
    articleList:[],
    hidden: true,                                                                        //隐藏表单控件
    hasMoreData: true,
  },
  bindinput: function (e) {
    //用户实时输入值
    var prefix = e.detail.value
    //匹配的结果
    var newSource = []
    if (prefix != "") { 
      // 对于数组array进行遍历，功能函数中的参数 `e`就是遍历时的数组元素值。
      this.data.adapterSource.forEach(function (e) { 
        // 用户输入的字符串如果在数组中某个元素中出现，将该元素存到newSource中
        if (e.indexOf(prefix) != -1) {
          console.log(e);
          newSource.push(e)
        }
      })
    };
    // 如果匹配结果存在，那么将其返回，相反则返回空数组
    if (newSource.length != 0) {
      this.setData({
        // 匹配结果存在，显示自动联想词下拉列表
        hideScroll: false,
        bindSource: newSource,
        arrayHeight: newSource.length * 71
      })
    } else {
      this.setData({
        // 匹配无结果，不现实下拉列表
        hideScroll: true,
        bindSource: []
      })
    }
  },
   // 用户点击选择某个联想字符串时，获取该联想词，并清空提醒联想词数组
  itemtap: function (e) {
    this.setData({
      // .id在wxml中被赋值为{{item}}，即当前遍历的元素值
      inputValue: e.target.id,
      // 当用户选择某个联想词，隐藏下拉列表
      hideScroll: true,
      bindSource: []
    })
  },
  /*点击导航栏 */
  onNavBarTap:function(event){
    // console.log(event.data.articleList.category_id);
    console.log('点击我')
    // 获取点击的navbar的index
    let navbarTapIndex=event.currentTarget.dataset.navbarIndex
    // 设置data属性中的navbarActiveIndex为当前点击的navbar
    this.setData({
      navbarActiveIndex: navbarTapIndex
    })
  },
  onBindAnimationFinish: function ({detail}) {
    // 设置data属性中的navbarActiveIndex为当前点击的navbar
    this.setData({
      navbarActiveIndex: detail.current
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
     wx.stopPullDownRefresh() //刷新完成后停止下拉刷新动效
    let _this=this
    wx.request({
      url:'https://www.haigeplus.cn/api/Category/list/',
      success:function(res){
          console.log(222,res.data.data.categoryList);
          _this.setData({
              categoryList:res.data.data.categoryList
          })  
      }
  }),
  wx.request({
    url:'https://www.haigeplus.cn/api/comment/list?article_id=21',
    success:function(res){
        console.log(res.data.data.commentData);
        _this.setData({
            commentList:res.data.data.commentData
        })  
    }
}),
utilList.request({
  url:'https://www.haigeplus.cn/api/Article/list/',
  data: {page:1},
  success:function(res){
      console.log(res);
      var date=[];
      for(let i=0;i<res.data.data.articleList.length;i++){
        date.push(util.formatTime(new Date(res.data.data.articleList[i].add_time*1000)));
      }
      _this.setData({
        articleList:res.data.data.articleList,
        date:date,
        page:1
      })  
      _this.onReachBottom()
  }
})

  },
  onReachBottom: function (){
    var that = this;
    wx.showLoading({
      title: '玩命加载中',
    })
    that.data.page = that.data.page+1;
    utilList.request({
      url: 'https://www.haigeplus.cn/api/Article/list/',
      //  method: 'GET',
      //  header: {'Content-Type': 'application/json' },
       data: {page:that.data.page},
       success: function(res) {
        console.log(res)
        const oldDate = that.data.articleList;
         if(res.data.status==0){
            that.setData({
              articleList : oldDate.concat(res.data.data.articleList)
            })
            wx.hideLoading();
         }
        }
    })
},
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this;
    that.setData({
      currentTab: 0 //当前页的一些初始数据，视业务需求而定
    })
    this.onLoad(); //重新加载onLoad()
  },

  /**
   * 页面上拉触底事件的处理函数
   */


  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    // var that=this;
    const { info, code } = that.data;
    return {
      title: info.name,
      path: `/pages/share/index?code=${code}&uid=${wx.getStorageSync('uid')}`,
      imageUrl: info.thumbnail_url
    }
  },
  onShareTimeline: function(){
    const { info, code } = that.data;
    return {
      title: info.name,
      query: `code=${code}&uid=${wx.getStorageSync('uid')}`,
      imageUrl: info.thumbnail_url
    }
  }
})